<?php
header('Content-Type: text/html; application/json');
class model
{
    protected $conn;
    public $arrlist = [];
    public function __construct()
    {
        $servername = "mariadb";
        $username = "root";
        $password = 123;
        $db = 'products';
        $this->conn = mysqli_connect($servername, $username, $password, $db);
    }

    function getData()
    {
        try {
            if (!$this->conn) {
                die('Could not connect:');
                return false;
            }
            $sql = "SELECT * FROM Item";
            $result =  mysqli_query($this->conn, $sql);
            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    $this->arrlist[] =
                        [
                            "intId" => $row["ID"],
                            "strName" => $row["Name"],
                            "strStatus" => $row["Status"]
                        ];
                }
            }
            return  $this->arrlist;
        } catch (Exception $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }

    //them item vao danh sach
    function postData($param, $arrlist)
    {
        try {
            print_r($arrlist);
            $intId = end($arrlist)['intId'] + 1;
            $strName = $param;
            if (!empty($strName)) {
                $sql = "INSERT INTO Item (ID,Name) VALUES ($intId,'$param')";
                $result =  mysqli_query($this->conn, $sql);
                if (!$result) {
                    echo "Error: " . $sql . "<br>" . mysqli_error($this->conn);
                    die;
                }
                echo "New record created successfully";
                $valueNode = [
                    'intId' => $intId,
                    'strName' => $strName,
                ];
                array_push($arrlist,$valueNode);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    function deleteData($param)
    {
        try {
            $sql = "DELETE FROM Item WHERE ID='$param'";
            $result =  mysqli_query($this->conn, $sql);

            if ($result) {
                if (!empty($param)) {
                    unset($arrItems[$param]);
                    echo "New record created successfully";
                }
            } else {
                echo "Error: " . $sql . "<br>" . mysqli_error($this->conn);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    function updateData($id, $status)
    {
        try {
            if (!empty($status)) {
                $sql = "UPDATE Item SET Status=$status WHERE ID=$id;";
                if ($this->conn->query($sql) === TRUE) {
                    echo "New record update successfully";
                } else {
                    echo "Error: " . $sql . "<br>" . mysqli_error($this->conn);
                }
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}
