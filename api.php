<?php
include 'model.php';

function addItem($param)
{
    $model = new model();
    $arrItem = $model->getData();
    $arr = $model->postData($param, $arrItem);
    try {

        if ($param != "") {
            $arrResponse = array(
                "success" => true,
                "code" => 200,
                "data" => $arr,
                "error" => NULL,
                "message" => ""
            );
        } else {
            $arrResponse = array(
                "success" => false,
                "code" => 404,
                "data" => null,
                "error" => "error",
                "message" => " Data not found"
            );
        }
        $json_response = json_encode($arrResponse);
        echo  $json_response;
    } catch (Exception $e) {
        echo $e->getMessage();
    }
}

//show item trong danh sach
function showItem()
{
    $model = new model();
    $arr = $model->getData();
    try {
        if ($arr != "") {
            $arrResponse = array(
                "success" => true,
                "code" => 200,
                "data" => $arr,
                "error" => NULL,
                "message" => ""
            );
        } else {
            $arrResponse = array(
                "success" => false,
                "code" => 404,
                "data" => null,
                "error" => "error",
                "message" => " Data not found"
            );
        }

        $json_response = json_encode($arrResponse);
        echo  $json_response;
    } catch (Exception $e) {
        echo $e->getMessage();
    }
}
//xoa phan tu truyen vao
function deleteItem($param)
{
    $model = new model();
    $arr = $model->deleteData($param);
    try {
        if (!empty($param)) {
            if ($arr != "") {
                $arrResponse = array(
                    "success" => true,
                    "code" => 200,
                    "data" => $arr,
                    "error" => NULL,
                    "message" => ""
                );
            }
            $arrResponse = array(
                "success" => false,
                "code" => 404,
                "data" => null,
                "error" => "error",
                "message" => " Data not found"
            );
        }
    } catch (Exception $e) {
        echo $e->getMessage();
    }
}

//update todo
function updateItem($id, $status)
{
    try {
        $model = new model();
        $arr = $model->updateData($id, $status);
        if ($arr != "") {
            $arrResponse = array(
                "success" => true,
                "code" => 200,
                "data" => $arr,
                "error" => NULL,
                "message" => ""
            );
        } else {
            $arrResponse = array(
                "success" => false,
                "code" => 404,
                "data" => null,
                "error" => "error",
                "message" => " Data not found"
            );
        }

        $json_response = json_encode($arrResponse);
        echo  $json_response;
    } catch (Exception $e) {
        echo $e->getMessage();
    }
}

//giup phan biet get value tra ve khi ajax goi
$request = $_GET['value'];
switch ($request) {
    case 'getData':
        showItem();
        break;
    case 'postData':
        addItem($_POST['strName']);
        break;
    case 'deleteData':
        deleteItem($_POST['intId']);
        break;
    case 'updateData':
        updateItem($_POST['status'], $_POST['intId']);
        break;
    default:
        "error request";
        break;
}
